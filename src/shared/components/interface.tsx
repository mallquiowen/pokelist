export interface IBasePokemon {
    name: string;
    url_picture: string;
    attack: string;
    defense:string;
  }
  export interface IPokemon extends IBasePokemon {
    id: number;

  }
  