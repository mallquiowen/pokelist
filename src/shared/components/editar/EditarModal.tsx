import React, { useState, useEffect } from "react";
import { IPokemon } from "./../interface";

interface IProps {
  editPokemon: IPokemon;
  onUpdatePokemon: (id: number, user: IPokemon) => void;
  setEdit: (bool: boolean) => void;
}

export default function EditarModal(props: IProps) {
  const [user, setUser] = useState(props.editPokemon);
  useEffect(() => setUser(props.editPokemon), [props]);
  const onFormSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!user.name) {
      return false;
    }
    props.onUpdatePokemon(user.id, user);
  };
  const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setUser({ ...user, [name]: value });
  };
  return (
    <div className="ontenedor__modal">
      <div className="ontenedor__modal__modalContent">
        <div>
          <h2 className="ontenedor__modal__modalContent__titulo">
            Editar Pokemon
          </h2>
        </div>
        <form onSubmit={onFormSubmit}>
          <div className="ontenedor__modal__modalContent__form">
            <div className="">
              <div className="ontenedor__modal__modalContent__form__text">
                <h5>Nombre:</h5>
                <input
                  type="text"
                  name="name"
                  value={user.name}
                  onChange={onInputChange}
                />
              </div>
              <div className="ontenedor__modal__modalContent__form__text">
                <h5>Imagen:</h5>
                <input
                  type="text"
                  name="url_picture"
                  onChange={onInputChange}
                  value={user.url_picture}
                />
              </div>
            </div>
            <div className="">
              <div className="ontenedor__modal__modalContent__form__text">
                <h5>Ataque</h5>
                <h6>0</h6>
                <input
                  type="range"
                  min="0"
                  max="100"
                  name="attack"
                  onChange={onInputChange}
                  value={user.attack}
                />
                <h6>100</h6>
              </div>
              <div className="ontenedor__modal__modalContent__form__text">
                <h5>Defensa</h5>
                <h6>0</h6>
                <input
                  type="range"
                  min="0"
                  max="100"
                  name="defense"
                  onChange={onInputChange}
                  value={user.defense}
                />
                <h6>100</h6>
              </div>
            </div>
          </div>
          <div className="ontenedor__modal__modalContent__botones">
            <button
              className="ontenedor__modal__modalContent__botones__guardar"
              type="submit"
            >
              Guardar
            </button>
            <button
              className="ontenedor__modal__modalContent__botones__cancelar"
              onClick={() => props.setEdit(false)}
            >
              Cancelar
            </button>
          </div>
        </form>
        <button
          className="ontenedor__modal__closeModal"
          onClick={() => props.setEdit(false)}
        >
          X
        </button>
      </div>
    </div>
  );
}
