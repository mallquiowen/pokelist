import React from "react";
import { IBasePokemon, IPokemon } from "../interface";
import "./table.scss";

const dateTable = [
  { title: "Nombre" },
  { title: "Imagen" },
  { title: "Ataque" },
  { title: "Defensa" },
  { title: "Acciones" },
];

interface IProps {
  datos: Array<IPokemon>;
  onEdit: (user: IPokemon) => void;
  onDelete: (user: IPokemon) => void;
}

const Table = (props: IProps) => {
  const { datos, onEdit, onDelete } = props;

  return (
    <>
      <div className="contenedor__table">
        <table>
          <thead>
            <tr>
              {dateTable.map((item, id) => {
                return <th key={id}>{item.title}</th>;
              })}
            </tr>
          </thead>
          <tbody>
            {props.datos.length > 0 ? (
              props.datos.map((pokemon,id) => (
                <tr key={id}>
                  <td>{pokemon.name}</td>
                  <td><img className="imagen" src={pokemon.url_picture} alt="" /></td>
                  <td>{pokemon.attack}</td>
                  <td>{pokemon.defense}</td>
                  <td>
                    <button
                      className="contenedor__table__editar"
                      onClick={() => props.onEdit(pokemon)}
                    >
                      Editar
                    </button>
                    <button
                      className="contenedor__table__eliminar"
                      onClick={() => props.onDelete(pokemon)}
                    >
                      Eliminar
                    </button>
                  </td>
                </tr>
              ))
            ) : (
              <tr>
                <td className="noDatos" colSpan={5}>
                  No hay datos para mostrar
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </>
  );
};
export default Table;
