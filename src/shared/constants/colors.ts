export const Colors = {
    primary: '#37559B',
    accent: '#252D60',
    secondary: '#252D60',
    tertiary: '#F56750',
    purple: '#26264F',
    lightPurple: '#91B3FA',
    gray: '#959494',
    grayLight: '#f7f7f7',
    grayDisabled: '#ffffff66',
    grayLightDark: '#B8B8B8',
    white: '#fff',
    red: '#FF1D25',
    skyBlue: '#078EC1',
    text: '#252D60',
    whatsapp: '#07A934',
    lightBlue: '#0084D4',
  };
  