import React, { useEffect, useState } from "react";
import { IBasePokemon, IPokemon } from "../../shared/components/interface";
import Table from "../../shared/components/table/Table";
import "./Home.scss";
import ModalNew from "./modalHome/ModalNew";
import EditarModal from "./../../shared/components/editar/EditarModal";

const pokemon = [
  {
    id: 1,
    name: "pikachu",
    url_picture:
      "https://th.bing.com/th/id/R.27b4012799d4f8ba5ce5d4f758b0cb5f?rik=mINFzHIGRI75Cg&riu=http%3a%2f%2fmuchotablet.com%2fwp-content%2fuploads%2f2016%2f08%2fpikachu.jpg&ehk=%2bVHkaxJZ%2bXzPrT1xacKICuLP8PooX%2b2nAOcyvODrCNg%3d&risl=&pid=ImgRaw&r=0",
    attack: "60",
    defense: "20",
  },
  {
    id: 2,
    name: "charmander",
    url_picture:
      "https://th.bing.com/th/id/OIP.waVBWPVy3_e_iJrZZ24vNgHaH0?pid=ImgDet&rs=1",
    attack: "80",
    defense: "10",
  },
  {
    id: 3,
    name: "bolbasor",
    url_picture:
      "https://th.bing.com/th/id/R.deb4d85327dc3de88e5fe0abe8021c6c?rik=%2fXet6nqSu3pSpg&riu=http%3a%2f%2fgetwallpapers.com%2fwallpaper%2ffull%2f2%2fa%2fa%2f355838.jpg&ehk=4tf%2bL%2fOX1Xg%2bt0u9wMTfUokt%2fkRA6xSIjQqpTnOX62E%3d&risl=&pid=ImgRaw&r=0",
    attack: "20",
    defense: "80",
  },
];

const Home = () => {
  const [openModalNew, setOpenModalNew] = useState(false);
  const [datos, setDatos] = useState(pokemon);
  const [editing, setEdit] = useState(false);
  const [editUser, setEditUser] = useState({
    name: "",
    url_picture: "",
    attack: "",
    defense: "",
    id: 10,
  });

  const openModal = () => setOpenModalNew(true);
  const newPokemon = () => {
    openModal();
  };

  // const getData = async () => {
  //   const url =
  //     "https://maral-app-service-6ycinmd6na-uc.a.run.app/maral/admin/pokemon-owen-all";
  //   const result = await fetch(url);
  //   const getResult = await result.json();
  //   setDatos(getResult);
  // };
  // useEffect(() => {
  //   getData();
  // }, []);

  const onAddPkemon = (newPokemon: IBasePokemon) => {
    const id = datos.length + 1;
    setDatos([...datos, { ...newPokemon, id }]);
  };

  const onEditPokemon = (user: IPokemon) => {
    setEditUser(user);
    setEdit(true);
  };
  const onUpdateUser = (id: number, newUser: IPokemon) => {
    setEdit(false);
    setDatos(datos.map((i) => (i.id === id ? newUser : i)));
  };

  const onDeletePokemon = (currentPokemon: IPokemon) => {
    setDatos(datos.filter((i) => i.id !== currentPokemon.id));
  };

  return (
    <>
      <div className="contenedor">
        <ul>
          <li className="active">
            <a>PokeList</a>
          </li>
        </ul>
        <div className="contenedor__titulo">
          <h3>Listado de Pokemon</h3>
        </div>
        <div className="contenedor__form">
          <input
            className="contenedor__form__buscar"
            type="text"
            placeholder="Buscar..."
          />
          <div>
            <button className="contenedor__form__boton" onClick={newPokemon}>
              +Nuevo
            </button>
          </div>
        </div>
        <Table
          datos={datos}
          onEdit={onEditPokemon}
          onDelete={onDeletePokemon}
        />
      </div>
      {editing ? (
        <EditarModal
          editPokemon={editUser}
          onUpdatePokemon={onUpdateUser}
          setEdit={setEdit}
        />
      ) : (
        <ModalNew
          modal={openModalNew}
          setModal={setOpenModalNew}
          onAddPkemon={onAddPkemon}
        />
      )}
    </>
  );
};

export default Home;
